#include "../stddef.h"

/* taken from 64-bit glibc; should be put into stddef.h
 * ssize_t is only internal, do not expose!
 */
typedef long int ssize_t;

extern ssize_t write(int fd, const char *text, size_t len);
