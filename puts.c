#include "include/stdio.h"
#include "include/private/lsyscall.h"

size_t strlen(const char *str)
{
	size_t i = 0;
	while (str[i]) {
		i++;
	}
	return i;
}

/*
int fputc(int c, FILE *stream) {
	return putc(c, stream->fd);
}

int putc(int c, FILE *stream) {
	mprint(stream->fd, &c, 1);
	return 0;
}

//int putchar(int c) {
//	return putc(c, stdout);
//}

//TODO
int puts(const char *s)
{
	int test = mprint(5, s, strlen(s));
	//putchar('\n');
	return 0;
}
*/


int fdputs(const char *s, int fd) {
	write(fd, s, strlen(s));

}
